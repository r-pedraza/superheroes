//
//  SuperHeroDetailImageTableViewCell.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import UIKit

class SuperHeroDetailImageTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var superHeroImageView: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
