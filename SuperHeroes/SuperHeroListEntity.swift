//
//  SuperHeroListEntity.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import ObjectMapper

class SuperHeroListEntity: Mappable {

    var superHeroesList: [SuperHeroEntity]?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        superHeroesList <- map["superheroes"]
    }
}
