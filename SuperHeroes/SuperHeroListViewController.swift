//
//  SuperHeroListViewController.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import UIKit

class SuperHeroListViewController: UIViewController {

    var presenter: SuperHeroListPresenterProtocol!
    var superHeroesList = [SuperHeroEntity]()
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        SuperHeroListConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    private func setupTableView() {
        self.title = NSLocalizedString("SUPER_HERO_LIST_TITLE", comment: "")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
        let nib = UINib(nibName: "SuperHeroListTableCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SuperHeroListTableCellID")
    }
}

extension SuperHeroListViewController: SuperHeroListViewProtocol {
    
    var tableViewReference: UITableView {
        return tableView
    }
}

extension SuperHeroListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return presenter.cellForRowAtIndexPath(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAtIndexPath(indexPath: indexPath)
    }

}
