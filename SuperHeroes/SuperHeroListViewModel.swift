//
//  SuperHeroListViewModel.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire

class SuperHeroListViewModel {
    
    var name: String?
    var photo: String?
    var image: UIImage?
    var superHeroDetailViewModel = SuperHeroDetailViewModel(cellType: .image)
    
    init(superHeroEntity: SuperHeroEntity) {
        self.name = superHeroEntity.name
        self.photo = superHeroEntity.photo
        self.superHeroDetailViewModel.photo = self.image
        self.superHeroDetailViewModel.nameValue = superHeroEntity.name
        self.superHeroDetailViewModel.nameTitle = superHeroEntity.name
        self.superHeroDetailViewModel.heightValue = superHeroEntity.height
        self.superHeroDetailViewModel.heightTitle = superHeroEntity.height
        self.superHeroDetailViewModel.skills = superHeroEntity.skills
        self.superHeroDetailViewModel.realNameValue = superHeroEntity.realName
        self.superHeroDetailViewModel.heightTitle = superHeroEntity.height
        self.superHeroDetailViewModel.power = superHeroEntity.power
        self.superHeroDetailViewModel.groups = superHeroEntity.groups
    }
}
