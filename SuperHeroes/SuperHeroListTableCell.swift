//
//  SuperHeroListTableCell.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import UIKit

class SuperHeroListTableCell: UITableViewCell {

    @IBOutlet weak var superHeroImageView: UIImageView!
    @IBOutlet weak var superHeroName: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.superHeroCellStyle()
    }
}
