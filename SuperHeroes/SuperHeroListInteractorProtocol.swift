//
//  SuperHeroListInteractorProtocol.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation

protocol SuperHeroListInteractorProtocol {
    var superHeroes: [SuperHeroListViewModel] { get }
    func objectAtIndex(indexPath: IndexPath) -> SuperHeroListViewModel
}
