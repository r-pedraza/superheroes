//
//  APIClient.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class APIClient {
    
   private static let URL = "https://api.myjson.com/bins/bvyob"
    
   static func loadSuperHeroescompletion(completion: @escaping (_ result: [SuperHeroEntity]) -> (Void))  {
         Alamofire.request(URL).responseObject { (response: DataResponse<SuperHeroListEntity>) in
            if let objectList = response.value?.superHeroesList {
                completion(objectList)
            }
        }
    }
    
    static func loadSuperHeroImage(superHero: SuperHeroListViewModel, completion: @escaping (_ result: UIImage) -> Void) {
        
        if let image = superHero.photo {
            Alamofire.request(image, method: .get).responseImage { response in
                guard let image = response.result.value else {
                    completion(#imageLiteral(resourceName: "placeholder"))
                    return
                }
                completion(image)
            }
        }
    }
}
