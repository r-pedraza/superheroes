//
//  SuperHeroListConfigurator.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation

class SuperHeroListConfigurator {
    
    static let sharedInstance: SuperHeroListConfigurator = {
        let instance = SuperHeroListConfigurator()
        return instance
    }()
    
    func configure(viewController: SuperHeroListViewController) {
        let routing = SuperHeroListRouting()
        routing.navigationController = viewController.navigationController
        
        let presenter = SuperHeroListPresenter()
        presenter.view = viewController
        presenter.routing = routing
        
        let interactor = SuperHeroListInteractor()
        interactor.presenter = presenter
        presenter.interactor = interactor
        viewController.presenter = presenter
    }
}
