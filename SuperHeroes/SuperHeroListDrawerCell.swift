//
//  SuperHeroListDrawerCell.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class SuperHeroListDrawerCell {
    static func cellForSuperHeroListViewModel(viewModel: SuperHeroListViewModel, tableView: UITableView) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SuperHeroListTableCellID") as? SuperHeroListTableCell else { return UITableViewCell() }
        cell.superHeroName.text = viewModel.name
        cell.superHeroImageView.image = #imageLiteral(resourceName: "placeholder").af_imageRoundedIntoCircle()
        APIClient.loadSuperHeroImage(superHero: viewModel, completion: {
             cell.superHeroImageView.image = $0.af_imageRoundedIntoCircle()
             viewModel.superHeroDetailViewModel.photo = $0.af_imageRoundedIntoCircle()
        })
        return cell
    }
}
