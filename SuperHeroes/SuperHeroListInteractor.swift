//
//  SuperHeroListInteractor.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class SuperHeroListInteractor: SuperHeroListInteractorProtocol {
    
    var presenter: SuperHeroListPresenterProtocol?
    var viewModels = [SuperHeroListViewModel]()
    
    init() {
        loadSuperHeroes()
    }
    
    func objectAtIndex(indexPath: IndexPath) -> SuperHeroListViewModel {
        return superHeroes[indexPath.row]
    }
        
    var superHeroes: [SuperHeroListViewModel] {
        return viewModels
    }
    
    //MARK: Private Methods
    
    private func buildViewModels(objects: [SuperHeroEntity]) {
       _ = objects.map({
            viewModels.append(SuperHeroListViewModel(superHeroEntity: $0))
        })
    }
    
    private func loadSuperHeroes() {
        _ = APIClient.loadSuperHeroescompletion(completion: {
            self.buildViewModels(objects: $0)
            self.presenter?.reloadData()
        })
    }
}
