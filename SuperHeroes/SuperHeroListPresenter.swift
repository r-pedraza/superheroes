//
//  SuperHeroListPresenter.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroListPresenter: SuperHeroListPresenterProtocol {
    var interactor: SuperHeroListInteractorProtocol!
    weak var view: SuperHeroListViewProtocol!
    var routing: SuperHeroListRouting!
    
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return interactor.superHeroes.count
    }
    
    func cellForRowAtIndexPath(indexPath: IndexPath) -> UITableViewCell {
        let object = interactor?.objectAtIndex(indexPath: indexPath)
        guard let viewModel = object else { return UITableViewCell() }
        return SuperHeroListFactoryCell.cellForObject(object: viewModel as AnyObject, tableView: view.tableViewReference)
    }
    
    func didSelectRowAtIndexPath(indexPath: IndexPath) {
        let superHero = interactor.objectAtIndex(indexPath: indexPath)
        routing.routingToSuperHeroDetailViewController(superHero: superHero)
    }
    
    func reloadData()  {
        view.tableViewReference.reloadData()
    }
}
