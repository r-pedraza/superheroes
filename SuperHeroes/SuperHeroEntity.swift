//
//  SuperHeroEntity.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import ObjectMapper

class SuperHeroEntity: Mappable {
  
    var name: String?
    var photo: String?
    var height:String?
    var skills:String?
    var realName:String?
    var power:String?
    var groups:String?
    
    var superHeroDetailEntity: SuperHeroDetailEntity?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        name <- map["name"]
        photo <- map["photo"]
        height <- map["height"]
        realName <- map["realName"]
        power <- map["power"]
        skills <- map["abilities"]
        groups <- map["groups"]
    }
}
