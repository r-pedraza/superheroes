//
//  SuperHeroListPresenterProtocol.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

protocol SuperHeroListPresenterProtocol {
    func numberOfSections() -> Int
    func numberOfRowsInSection(section: Int) -> Int
    func cellForRowAtIndexPath(indexPath: IndexPath) -> UITableViewCell
    func didSelectRowAtIndexPath(indexPath: IndexPath)
    func reloadData()
}
