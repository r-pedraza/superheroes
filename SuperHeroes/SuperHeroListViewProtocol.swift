//
//  SuperHeroListViewProtocol.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

protocol SuperHeroListViewProtocol: class {
    
    var tableViewReference: UITableView { get }
    
}
