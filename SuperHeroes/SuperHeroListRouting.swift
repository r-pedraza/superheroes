//
//  SuperHeroListRouting.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroListRouting: SuperHeroListRoutingProtocol {
    
    var navigationController: UINavigationController?
    
    func routingToSuperHeroDetailViewController(superHero: SuperHeroListViewModel) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let viewController = storyboard.instantiateViewController(withIdentifier: "SuperHeroDetailViewControllerID") as? SuperHeroDetailViewController {
            viewController.superHero = superHero.superHeroDetailViewModel
            routingToSuperHeroDetail(viewController: viewController, superHero: superHero)
        }
    }
    
    func routingToSuperHeroDetail(viewController: SuperHeroDetailViewController, superHero: SuperHeroListViewModel) {
        if let navigationController = viewController.navigationController {
            self.navigationController = navigationController
        }
        let interactor = SuperHeroDetailInteractor(superHero: superHero)
        let routing = SuperHeroDetailRouting()
        routing.navigationController = navigationController
        let presenter = SuperHeroDetailPresenter()
        presenter.interactor = interactor
        presenter.view = viewController
        presenter.routing = routing
        viewController.presenter = presenter
        interactor.presenter = presenter
        navigationController?.pushViewController(viewController, animated: true)
    }
}
