//
//  CellConstants.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation

struct CellConstants {
    
    enum CellXib: String {
        case SuperHeroListCell  = "SuperHeroListTableCell"
    }
    
    enum CellIdentifier: String {
        case SuperHeroListCell  = "SuperHeroListTableCellID"
    }
    
}

