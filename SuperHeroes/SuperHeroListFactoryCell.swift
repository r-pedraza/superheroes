//
//  SuperHeroListFactoryCell.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 23/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroListFactoryCell {
    
    static func cellForObject(object: AnyObject, tableView: UITableView) -> UITableViewCell {
        if object is  SuperHeroListViewModel{
            guard let viewModel = object as?  SuperHeroListViewModel else { return UITableViewCell() }
            return SuperHeroListDrawerCell.cellForSuperHeroListViewModel(viewModel: viewModel, tableView: tableView)
        }
    return UITableViewCell()
    }
}
