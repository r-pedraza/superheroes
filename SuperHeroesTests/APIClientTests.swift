//
//  APIClientTests.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 25/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import XCTest

class APIClientTests: XCTestCase {
    
    func testGetSuperHeroes() {
        let wait = expectation(description: "Get some superheroes!")
        APIClient.loadSuperHeroescompletion(completion: {
            XCTAssertTrue($0.count > 0)
            wait.fulfill()
        })
        waitForExpectations(timeout: 10, handler: nil)
    }
    
}
