//
//  SuperHeroListInteractorTests.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 25/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import XCTest
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

class SuperHeroListInteractorTests: XCTestCase {
    

    //MARK: Private Methods
    private func loadSuperHeroList() -> [SuperHeroEntity] {
        let JSON = readJson()
        let map = Map(mappingType: .fromJSON, JSON: JSON)
        let superHeroList = SuperHeroListEntity(map: map)
        superHeroList?.mapping(map: map)
        guard let superHeroListToReturn = superHeroList?.superHeroesList else { return [SuperHeroEntity]() }
        return superHeroListToReturn
    }
    
    
    private func readJson() -> [String: Any]  {
        var responseDic = [String: Any]()
        do {
            if let file = Bundle.main.url(forResource: "superHeroesResponse", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    responseDic = object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return responseDic
    }
}
