//
//  SuperHeroDetailTests.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 25/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import XCTest
import AlamofireObjectMapper
import Alamofire
import ObjectMapper

class SuperHeroDetailTests: XCTestCase {
    
    func testSuperHeroDetailName() {
        let superHeroList = loadSuperHeroList()
        if let superHeroEntity = superHeroList.first {
            let viewModel = SuperHeroListViewModel(superHeroEntity: superHeroEntity)
            let interactor = SuperHeroDetailInteractor(superHero: viewModel)
            let detailViewModel = interactor.superHero
            XCTAssertEqual(detailViewModel.name, superHeroEntity.name)
        }
    }
    
    func testSuperHeroObjectAtIndex() {
        let superHeroList = loadSuperHeroList()
        if let entity = superHeroList.first {
           let listViewModel = SuperHeroListViewModel(superHeroEntity: entity)
           let interactor = SuperHeroDetailInteractor(superHero: listViewModel)
            let indexpath = IndexPath(item: 0, section: 0)
            let viewModel = interactor.objectAtIndex(indexPath: indexpath)
            let lastViewModel = interactor.superHeroes.last
            XCTAssertTrue(viewModel.cellType == .image)
            XCTAssertTrue(lastViewModel?.cellType == .info)
        }
    }
    
    func testSuperHeroBuilderDataSource() {
        let superHeroList = loadSuperHeroList()
        if let entity = superHeroList.first {
            let listViewModel = SuperHeroListViewModel(superHeroEntity: entity)
            let interactor = SuperHeroDetailInteractor(superHero: listViewModel)
            let interactorList = interactor.superHeroes
            let dataSource = SuperHeroDetailBuilderDataSource.buildSuperHeroDetail(superHero: listViewModel)
            XCTAssertEqual(interactorList.count, dataSource.count)
        }
    }
    
    private func loadSuperHeroList() -> [SuperHeroEntity] {
        let JSON = readJson()
        let map = Map(mappingType: .fromJSON, JSON: JSON)
        let superHeroList = SuperHeroListEntity(map: map)
        superHeroList?.mapping(map: map)
        guard let superHeroListToReturn = superHeroList?.superHeroesList else { return [SuperHeroEntity]() }
        return superHeroListToReturn
    }
    
    
    private func readJson() -> [String: Any]  {
        var responseDic = [String: Any]()
        do {
            if let file = Bundle.main.url(forResource: "superHeroesResponse", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    responseDic = object
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        return responseDic
    }
}
