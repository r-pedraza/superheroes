//
//  SuperHeroDetailEntity.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation

class SuperHeroDetailEntity {
     var height:String?
     var skills:String?
     var realName:String?
     var power:String?
     var groups:String?
    
    required init(superHero: SuperHeroEntity) {
        self.height = superHero.height
        self.skills = superHero.skills
        self.realName = superHero.realName
        self.power = superHero.power
        self.groups = superHero.groups
    }
}
