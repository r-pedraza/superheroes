//
//  SuperHeroDetailDrawerCell.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroDetailDrawerCell {
    
    static func cellImage(viewModel: SuperHeroDetailViewModel, tableView: UITableView)-> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SuperHeroDetailImageTableViewCellID") as? SuperHeroDetailImageTableViewCell else { return UITableViewCell() }
        cell.superHeroImageView.image = viewModel.photo
        return cell
    }
    
    static func cellTwoLabels(viewModel: SuperHeroDetailViewModel, tableView: UITableView)-> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SuperHeroDetailTwoLabelsTableViewCellID") as? SuperHeroDetailTwoLabelsTableViewCell else { return UITableViewCell() }
        cell.titleLabel.text = viewModel.title
        cell.valueLabel.text = viewModel.value
        return cell
    }
    
    static func cellInfo(viewModel: SuperHeroDetailViewModel, tableView: UITableView)-> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SuperHeroDetailInfoTableViewCellID") as? SuperHeroDetailInfoTableViewCell else { return UITableViewCell() }
        cell.infoTitleLabel.text = viewModel.title
        cell.infoTextLabel.text = viewModel.value
        return cell
    }
}
