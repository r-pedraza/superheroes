//
//  SuperHeroDetailViewModel.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

enum CellType {
    case image
    case twoLabels
    case info
}

class SuperHeroDetailViewModel {
    var nameValue: String?
    var nameTitle: String?
    var photo: UIImage?
    var realNameTitle: String?
    var realNameValue: String?
    var heightTitle: String?
    var heightValue: String?
    var power: String?
    var skills: String?
    var groups: String?
    var title: String?
    var value: String?
    var cellType: CellType
    
    init(cellType: CellType) {
        self.cellType = cellType
    }
    
}
