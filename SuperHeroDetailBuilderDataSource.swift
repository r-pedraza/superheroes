//
//  SuperHeroDetailBuilderDataSource.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation

struct SuperHeroDetailBuilderDataSource {
    
    static func buildSuperHeroDetail(superHero: SuperHeroListViewModel) -> [SuperHeroDetailViewModel] {
        var dataSource = [SuperHeroDetailViewModel]()
       
        let superHeroImageViewModel = SuperHeroDetailViewModel(cellType: .image)
        superHeroImageViewModel.photo = superHero.superHeroDetailViewModel.photo ?? #imageLiteral(resourceName: "placeholder")
        dataSource.append(superHeroImageViewModel)

        let nameViewModel = SuperHeroDetailViewModel(cellType: .twoLabels)
        nameViewModel.title = NSLocalizedString("NAME_TITLE", comment: "")
        nameViewModel.value = superHero.name
        dataSource.append(nameViewModel)

        
        let realNameViewModel = SuperHeroDetailViewModel(cellType: .twoLabels)
        realNameViewModel.title = NSLocalizedString("REAL_NAME_TITLE", comment: "")
        realNameViewModel.value = superHero.superHeroDetailViewModel.realNameValue
        dataSource.append(realNameViewModel)
        
        let heightViewModel = SuperHeroDetailViewModel(cellType: .twoLabels)
        heightViewModel.title = NSLocalizedString("HEIGHT_TITLE", comment: "")
        heightViewModel.value = superHero.superHeroDetailViewModel.heightValue
        dataSource.append(heightViewModel)
     
        let powerViewModel = SuperHeroDetailViewModel(cellType: .info)
        powerViewModel.title = NSLocalizedString("POWER_TITLE", comment: "")
        powerViewModel.value = superHero.superHeroDetailViewModel.power
        dataSource.append(powerViewModel)
        
        let skillsViewModel = SuperHeroDetailViewModel(cellType: .info)
        skillsViewModel.title = NSLocalizedString("SKILLS_TITLE", comment: "")
        skillsViewModel.value = superHero.superHeroDetailViewModel.skills
        dataSource.append(skillsViewModel)
        
        let groupViewModel = SuperHeroDetailViewModel(cellType: .info)
        groupViewModel.title = NSLocalizedString("GROUPS_TITLE", comment: "")
        groupViewModel.value = superHero.superHeroDetailViewModel.groups
        dataSource.append(groupViewModel)
        
        return dataSource
    }
}
