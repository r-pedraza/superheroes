//
//  SuperHeroDetailRoutingProtocol.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

protocol SuperHeroDetailRoutingProtocol {
    func routingToSuperHeoresList()
    
}
