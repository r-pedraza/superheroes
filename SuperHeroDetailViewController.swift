//
//  SuperHeroDetailViewController.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import UIKit

class SuperHeroDetailViewController: UIViewController, SuperHeroDetailViewProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    var superHero:SuperHeroDetailViewModel?
    var presenter: SuperHeroDetailPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: Private Methods
    
    private func setupUI() {
        self.title = superHero?.nameValue
        setupTableView()
    }

    private func setupTableView()  {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 80
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableViewAutomaticDimension
        tableViewRegisterCells()
    }
    
    private func tableViewRegisterCells() {
        let nibInfo = UINib(nibName: "SuperHeroDetailInfoTableViewCell", bundle: nil)
        tableView.register(nibInfo, forCellReuseIdentifier: "SuperHeroDetailInfoTableViewCellID")
        let nibImage = UINib(nibName: "SuperHeroDetailImageTableViewCell", bundle: nil)
        tableView.register(nibImage, forCellReuseIdentifier: "SuperHeroDetailImageTableViewCellID")
        let nibTwoLabels = UINib(nibName: "SuperHeroDetailTwoLabelsTableViewCell", bundle: nil)
        tableView.register(nibTwoLabels, forCellReuseIdentifier: "SuperHeroDetailTwoLabelsTableViewCellID")
    }
    
    var tableViewReference: UITableView {
        return tableView
    }
    
    //MARK: Actions
    
    @IBAction func didTapInBackButton(_ sender: Any) {
        presenter.didTapInBackButton()
    }

}

extension SuperHeroDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return presenter.cellForRowAtIndexPath(indexPath: indexPath)
    }
}
