//
//  SuperHeroDetailInteractor.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation

class SuperHeroDetailInteractor: SuperHeroDetailInteractorProtocol {
    var presenter: SuperHeroDetailPresenterProtocol?
    var superHero: SuperHeroListViewModel
    
   required init(superHero: SuperHeroListViewModel) {
        self.superHero = superHero
    }
    
    var superHeroes: [SuperHeroDetailViewModel] {
        return SuperHeroDetailBuilderDataSource.buildSuperHeroDetail(superHero: superHero)
    }
    
    func objectAtIndex(indexPath: IndexPath) -> SuperHeroDetailViewModel {
        return superHeroes[indexPath.row]
    }
}
