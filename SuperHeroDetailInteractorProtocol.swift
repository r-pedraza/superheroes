//
//  SuperHeroDetailInteractorProtocol.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation

protocol SuperHeroDetailInteractorProtocol {
    var superHeroes: [SuperHeroDetailViewModel] { get }
    func objectAtIndex(indexPath: IndexPath) -> SuperHeroDetailViewModel
}
