//
//  SuperHeroDetailPresenter.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroDetailPresenter: SuperHeroDetailPresenterProtocol {
    var interactor: SuperHeroDetailInteractorProtocol!
    weak var view: SuperHeroDetailViewProtocol!
    var routing: SuperHeroDetailRoutingProtocol!
    
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRowsInSection() -> Int {
        return interactor.superHeroes.count
    }
    
    func cellForRowAtIndexPath(indexPath: IndexPath) -> UITableViewCell {
        let object = interactor?.objectAtIndex(indexPath: indexPath)
        guard let viewModel = object else { return UITableViewCell() }
        return SuperHeroDetailFactoryCell.cellForObject(viewModel: viewModel, tableView: view.tableViewReference)
    }
    
    func reloadData()  {
        view.tableViewReference.reloadData()
    }
    
    func didTapInBackButton() {
        routing.routingToSuperHeoresList()
    }
}
