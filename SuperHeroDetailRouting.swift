//
//  SuperHeroDetailRouting.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroDetailRouting: SuperHeroDetailRoutingProtocol {

    var navigationController: UINavigationController?
    
    func routingToSuperHeoresList() {
        navigationController?.popViewController(animated: true)
    }

}
