//
//  SuperHeroDetailFactoryCell.swift
//  SuperHeroes
//
//  Created by RaúlPedraza on 24/08/2017.
//  Copyright © 2017 RaúlPedraza. All rights reserved.
//

import Foundation
import UIKit

class SuperHeroDetailFactoryCell {
    static func cellForObject(viewModel: SuperHeroDetailViewModel, tableView: UITableView) -> UITableViewCell {
        switch viewModel.cellType {
        case .image:
            return SuperHeroDetailDrawerCell.cellImage(viewModel: viewModel, tableView: tableView)
        case .twoLabels:
            return SuperHeroDetailDrawerCell.cellTwoLabels(viewModel: viewModel, tableView: tableView)
        case .info:
            return SuperHeroDetailDrawerCell.cellInfo(viewModel: viewModel, tableView: tableView)            
        }
    }
}
